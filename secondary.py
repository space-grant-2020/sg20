from datetime import datetime
from picamera import PiCamera
from time import sleep
#Initialize camera
camera = PiCamera()
#Rotate camera as needed (0, 90, 180, or 270)
camera.rotation = 0
#File path of video file
dir = '/home/pi/'
#Record video in 300 second (5 minute) blocks
while True:
    now = datetime.now()
    name = now.strftime('%H:%M:%S')
    camera.start_recording(dir + name)
    sleep(300)
    camera.stop_recording()
